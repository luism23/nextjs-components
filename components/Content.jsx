import Head from "next/head";

import Header from "@/components/Header";
import Footer from "@/components/Footer";

const Index = ({ children, header = true, footer = true,title="" }) => {
    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            {header && <Header />}
            <div className="content">{children}</div>
            {footer && <Footer />}
        </>
    );
};
export default Index;
