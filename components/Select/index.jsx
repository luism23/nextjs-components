import SelectBase from "@/components/Select/Base";

const Index = (props) => {
    const config = { ...props };
    config.classNameInput = `
        p-h-10 p-v-5 
        font-20 font-montserrat
        width-p-100 
        border-0 
        border-radius-0
        box-shadow box-shadow-inset box-shadow-s-1 box-shadow-black 
        outline-none 
    `;
    config.classNameLabel = `
        pos-r 
        d-block 
        font-20 font-montserrat
    `;
    config.classNameError = `
        font-10 font-montserrat
        text-right
        p-v-3
        color-error
    `;
    config.classNameOptions = `
        pos-a 
        top-p-100 left-0 
        width-p-100
        m-0
        p-v-10 p-h-0
        bg-white
        box-shadow box-shadow-inset box-shadow-s-1 box-shadow-black 
        font-15
        z-index-1
    `;
    config.classNameOption = `
        p-h-15
        bg-gray-hover
        color-white-hover
        pointer
        list-none
    
    `;
    config.classNameOptionDisabled = `
        bg-gray
        color-white
    `

    return (
        <>
            <SelectBase {...config} />
        </>
    );
};
export default Index;
