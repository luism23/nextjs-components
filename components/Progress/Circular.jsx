const Index = ({ p = 50, children, posPor = "before" }) => {
    p = Math.min(Math.max(p, 0), 100);

    const classNameProgressCircular = `
        progress-circular 
        width-p-100 
        pos-r 
        flex flex-justify-center flex-align-center 
        text-center 
        font-montserrat 
        box-shadow
        box-shadow-inset
        box-shadow-c-gray
        box-shadow-s-5
        border-radius-p-100
    `;
    const classNameProcentaje = `
        progress-circular-porcentaje 
        pos-a
        top-0 
        left-0 
        right-0 
        bottom-0 
        m-auto 
        width-p-100 
        flex 
        flex-justify-center 
        flex-align-content-center
        ${posPor}
    `;
    const classNameCap = `
        cap
        pos-a 
        top-0 
        width-p-50 
        height-p-100 
        overflow-hidden
    `;
    const classNameCapCircle = `
        capCircle 
        border-5 
        border-style-solid 
        border-bunker 
        border-t-transparent 
        border-r-transparent
        border-radius-p-100 
        pos-a 
        top-0
        width-p-100 
        height-p-100 
    `;
    return (
        <>
            <div
                className={classNameProgressCircular}
                style={{ "--p": p, paddingBottom: "100%" }}
            >
                <span className={classNameProcentaje}>{children}</span>
                <div className={`${classNameCap} cap1 right-0`}>
                    <div
                        className={`${classNameCapCircle} right-0`}
                        style={{
                            width: "200%",
                        }}
                    />
                </div>
                <div className={`${classNameCap} cap2 left-0`}>
                    <div
                        className={`${classNameCapCircle} left-0`}
                        style={{
                            width: "200%",
                            "--min": 50,
                            "--max": 100,
                        }}
                    />
                </div>
            </div>
        </>
    );
};
export default Index;
