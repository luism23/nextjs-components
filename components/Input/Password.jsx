import { useState } from "react";

import Input from "@/components/Input";

import SVGEye from "@/svg/eye";

const Index = (props) => {
    const [type, setType] = useState("password");
    const toggleTypePassword = () => {
        setType(type == "password" ? "text" : "password");
    };
    const propsPassword = {
        classNameContentInput: `
            pos-r
        `,
        icon: (
            <>
                <span
                    onClick={toggleTypePassword}
                    className={`
                    pos-a
                    top-0 
                    right-10
                    bottom-0
                    m-auto
                    flex flex-align-center
                    pointer
                    z-index-3
                    color-black color-blue-hover
                `}
                >
                    <SVGEye/>
                </span>
            </>
        ),
    };
    return (
        <>
            <Input {...props} {...propsPassword} type={type} />
        </>
    );
};
export default Index;
