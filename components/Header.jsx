import { useState, useEffect, useRef } from "react";
import Link from "next/link";

import Menu from "@/components/Menu";
import Menu2 from "@/components/Menu/Menu2";

import DATA from "@/data/components/Header";

const Index = () => {
    const header = useRef(null);
    const [height, setHeight] = useState(0);

    const loadHeightTopPaddingBody = () => {
        const heightMenu = header.current.offsetHeight;
        setHeight(heightMenu);
        document.body.style.setProperty("--sizeHeader", `${heightMenu}px`);
    };
    const loadHeader = () => {
        loadHeightTopPaddingBody();
    };
    useEffect(() => {
        if (header) {
            loadHeader();
        }
    }, [header]);
    return (
        <>
            <header
                ref={header}
                className={`
                    header 
                    pos-f top-0 left-0 
                    width-p-100 
                    flex flex-align-center
                    p-v-15 
                    z-index-9
                    bg-white
                    box-shadow box-shadow-black box-shadow-b-1
                `}
            >
                <div className="container p-h-15 flex flex-align-center">
                    <div className="flex-3">
                        <Link href="/">
                            <a
                                className={`
                                color-black color-blue-hover
                                font-16 font-montserrat
                            `}
                            >
                                Template Nextjs
                            </a>
                        </Link>
                    </div>
                    <div className="flex-6">
                        <Menu items={DATA.nav} />
                    </div>
                    <div className="flex-3 flex flex-justify-right">
                        <Menu2 items={DATA.nav2} />
                    </div>
                </div>
            </header>
        </>
    );
};
export default Index;
