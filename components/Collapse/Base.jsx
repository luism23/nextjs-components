import { useState } from "react";

import SVGArrowCollapse from "@/svg/arrowCollapse";

const Index = ({
    children,
    header,
    className = "",
    classNameActive = "",
    classNameNotActive = "",
    classNameHeader = "",
    classNameHeaderActive = "",
    classNameHeaderNotActive = "",
    classNameBody = "",
    classNameBodyActive = "",
    classNameBodyNotActive = "",
    classNameIcon = "",
    classNameIconActive = "",
    classNameIconNotActive = "",
    icon = <SVGArrowCollapse />,
}) => {
    const [active, setActive] = useState(false);
    return (
        <>
            <div
                className={`${className} ${
                    active ? classNameActive : classNameNotActive
                }`}
            >
                <div
                    onClick={() => {
                        setActive(!active);
                    }}
                    className={`${classNameHeader} ${
                        active
                            ? classNameHeaderActive
                            : classNameHeaderNotActive
                    }`}
                >
                    <div>{header}</div>
                    <span
                        className={`${classNameIcon} ${
                            active
                                ? classNameIconActive
                                : classNameIconNotActive
                        }`}
                    >
                        {icon}
                    </span>
                </div>
                <div
                    className={`${classNameBody} ${
                        active ? classNameBodyActive : classNameBodyNotActive
                    }`}
                >
                    {children}
                </div>
            </div>
        </>
    );
};
export default Index;
