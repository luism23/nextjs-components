import CollapseBase from "@/components/Collapse/Base";

const Index = (props) => {
    const config = { ...props };
    config.className = `
        border-radius-10
        box-shadow box-shadow-b-3 box-shadow-c-gray
        p-v-10 p-h-15
    `;
    config.classNameHeader = `
        flex flex-align-center flex-justify-between
        font-montserrat font-18
        color-gray
        pointer
    `;
    config.classNameHeaderActive = `
        color-bunker
    `;
    config.classNameIcon = `
        transition-5
    `;
    config.classNameIconActive = `
        rotate-180
    `;
    config.classNameBody = `
        font-12 font-montserrat
        color-gray
        overflow-hidden
        transition-5
    `
    config.classNameBodyActive = `
        border-0 border-t-1 border-style-solid
        p-t-10
        m-t-10
        height-vh-max-100
    `
    config.classNameBodyNotActive = `
        height-max-0
    `
    return <CollapseBase {...config} />;
};
export default Index;
