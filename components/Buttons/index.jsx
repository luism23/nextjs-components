import ButtonBase from "@/components/Buttons/Base";

const Index = (props) => {
    const config = { ...props };
    config.className = `
        width-p-100 
        p-h-15 p-v-10
        font-18 font-w-900 line-h-8
        font-montserrat
        color-black color-white-hover
        border-radius-80
        border-4 border-style-solid border-black
        bg-white bg-black-hover
        flex flex-justify-center flex-align-center
        text-center
    `;
    config.classNameLoader = `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
    `;
    return <ButtonBase {...config} />;
};
export default Index;
