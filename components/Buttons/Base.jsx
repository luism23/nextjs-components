const Index = ({
    children,
    loader = false,
    disabled = false,
    onClick = () => {},
    className = "",
    classNameLoader = "",
    icon = "",
    iconPosition = "left",
}) => {
    return (
        <>
            <button
                onClick={onClick}
                className={className}
                disabled={loader || disabled}
            >
                {iconPosition == "left" && icon}
                {loader ? <div className={classNameLoader}></div> : children}
                {iconPosition == "right" && icon}
            </button>
        </>
    );
};
export default Index;
