import MenuBase from "@/components/Menu/Base";

const Index = (props) => {
    const config = { ...props };


    config.classNameContent = `
        width-p-100
        flex flex-justify-right
        pos-r
    `
    config.classNameButton = `
        border-0
        bg-transparent
        color-black color-blue-hover
    `
    config.classNameUl = `
        p-0
        m-0
        flex flex-justify-center
        pos-a
        top-p-100
        height-vh-max-0
        overflow-hidden
        z-index-9
        box-shadow box-shadow-black box-shadow-b-1
        border-radius-10
    `;
    config.classNameUlActive = `
        p-h-20 p-v-10
        height-vh-max-100
        flex-column flex-nowrap flex-align-start
        bg-white
        overflow-auto
    `;
    config.classNameLi = `
        flex
        font-16 font-montserrat
    `;
    config.classNameLiActive = `
        p-v-5
    `;
    config.classNameA = `
        p-v-5 p-h-10
        color-black color-blue-hover
    `;

    return (
        <>
            <MenuBase {...config} />
        </>
    );
};
export default Index;
