import MenuBase from "@/components/Menu/Base";

const Index = (props) => {
    const config = { ...props };

    const responsive = "lg"

    config.classNameContent = `
        flex flex-justify-center
        pos-r
    `
    config.classNameButton = `
        d-${responsive}-none
        border-0
        bg-transparent
        color-black color-blue-hover
    `
    config.classNameUl = `
        p-0
        m-0
        flex flex-justify-center
        pos-${responsive}-r pos-a
        top-p-100
        height-${responsive}-p-max-100  height-vh-max-0
        overflow-hidden
        z-index-9
    `;
    config.classNameUlActive = `
        height-vh-max-100
        width-vw-100
        flex-column flex-nowrap flex-align-center
        bg-white
        overflow-auto
    `;
    config.classNameLi = `
        flex
        font-16 font-montserrat
    `;
    config.classNameLiActive = `
        p-v-25
    `;
    config.classNameA = `
        p-v-5 p-h-10
        color-black color-blue-hover
    `;

    return (
        <>
            <MenuBase {...config} />
        </>
    );
};
export default Index;
