import Link from "next/link";
import { useState, useEffect, useRef } from "react";

import SVGBars from "@/svg/bars";
import SVGClose from "@/svg/close";

const Index = ({
    items = [],
    classNameContent = "",
    classNameButton = "",
    classNameUl = "",
    classNameUlActive = "",
    classNameLi = "",
    classNameLiActive = "",
    classNameA = "",
}) => {
    const [active, setActive] = useState(false);
    const menu = useRef(null);

    const toggleMenu = () => {
        setActive(!active);
    };
    const loadEnventBody = () => {
        document.addEventListener("click", function (event) {
            if (menu?.current) {
                var isClickInsideElement = menu.current.contains(event.target);
                if (!isClickInsideElement ) {
                    setActive(false);
                }
            }
        });
    };
    useEffect(() => {
        loadEnventBody();
    }, []);
    return (
        <>
            <div ref={menu} className={classNameContent}>
                <button className={classNameButton} onClick={toggleMenu}>
                    <span className={`${active?"d-none":""}`}><SVGBars /></span>
                    <span className={`${!active?"d-none":""}`}><SVGClose /></span>
                </button>
                <ul
                    className={`${classNameUl} ${
                        active ? classNameUlActive : ""
                    }`}
                >
                    {items.map((e, i) => {
                        return (
                            <li
                                key={i}
                                className={`${classNameLi} ${
                                    active ? classNameLiActive : ""
                                }`}
                            >
                                <Link href={e.link}>
                                    <a className={classNameA}>{e.text}</a>
                                </Link>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </>
    );
};
export default Index;
