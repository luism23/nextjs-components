import axios from "axios";
import log from "@/functions/log";

const request = async (config) => {
    try {
        const response = await axios(config);
        log("RESPOND REQUEST", response, "green");
        return response.data;
    } catch (error) {
        log("ERROR REQUEST", error, "red");
        return error.response.data;
    }
};
export default request;
