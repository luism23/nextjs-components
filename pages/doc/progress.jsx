import { useState } from "react";

import Content from "@/components/Content";
import Sticky from "@/components/Sticky";
import ProgressLinear from "@/components/Progress/Linear";
import ProgressCircular from "@/components/Progress/Circular";
import LinkReadme from "@/components/LinkReadme";
import Image from "@/components/Image";

import log from "@/functions/log";

const Index = () => {
    return (
        <>
            <Content title="Progress">
                <div className="container p-15 flex flex-justify-between  flex-gap-0 flex-md-gap-15">
                    <div className="flex-12 flex-md-6 flex-lg-4">
                        <Sticky className="p-v-20">
                            <ul className="m-0">
                                <li>
                                    <a
                                        href="#import-required"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Import Required
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#progress-linear"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Progress Linear
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#progress-circular"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Progress Circular
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#progress-circular-with-content"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Progress Circular with Content
                                    </a>
                                </li>
                            </ul>
                        </Sticky>
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-8">
                        <div>
                            <div
                                id="import-required"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Import Required
                                <LinkReadme id="progress-import-required" />
                            </h1>
                            <Image
                                src="/code/Progress/ProgressRequired.png"
                                className="m-b-100"
                            />
                        </div>
                        <div>
                            <div
                                id="progress-linear"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Progress Linear
                                <LinkReadme id="progress-linear" />
                            </h1>
                            <ProgressLinear p={76} />
                            <Image
                                src="/code/Progress/ProgressLinear.png"
                                className="m-b-100 m-t-20"
                            />
                        </div>
                        <div>
                            <div
                                id="progress-circular"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Progress Circular
                                <LinkReadme id="collapse" />
                            </h1>
                            <div className="flex flex-justify-around">
                                <div className="width-50">
                                    <ProgressCircular p={65} />
                                </div>
                                <div className="width-50">
                                    <ProgressCircular p={10} />
                                </div>
                                <div className="width-50">
                                    <ProgressCircular p={95} />
                                </div>
                                <div className="width-50">
                                    <ProgressCircular p={35} />
                                </div>
                            </div>
                            <Image
                                src="/code/Progress/ProgressCircular.png"
                                className="m-b-100 m-t-20"
                            />
                        </div>
                        <div>
                            <div
                                id="progress-circular-with-content"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Progress Circular with Content
                                <LinkReadme id="collapse" />
                            </h1>
                            <div className="flex flex-justify-around">
                                <div style={{ width: "200px" }}>
                                    <ProgressCircular p={65}>
                                        <div className="width-p-100">Text</div>
                                    </ProgressCircular>
                                </div>
                                <div style={{ width: "200px" }}>
                                    <ProgressCircular p={45} posPor="after">
                                        <div className="width-p-100">Text</div>
                                    </ProgressCircular>
                                </div>
                            </div>
                            <Image
                                src="/code/Progress/ProgressCircularWithContent.png"
                                className="m-b-100 m-t-20"
                            />
                        </div>
                    </div>
                </div>
            </Content>
        </>
    );
};
export default Index;
