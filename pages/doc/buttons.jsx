import { useState } from "react";

import Content from "@/components/Content";
import Sticky from "@/components/Sticky";
import Buttons from "@/components/Buttons";
import LinkReadme from "@/components/LinkReadme";
import Image from "@/components/Image";

import log from "@/functions/log";

import SVGEye from "@/svg/eye";

const Index = () => {
    const [loader, setLoader] = useState(false);
    const clickBtn = () => {
        setLoader(true);
        setTimeout(() => {
            setLoader(false);
        }, 1000);
    };
    return (
        <>
            <Content title="Inputs">
                <div className="container p-15 flex flex-justify-between  flex-gap-0 flex-md-gap-15">
                    <div className="flex-12 flex-md-6 flex-lg-4">
                        <Sticky className="p-v-20">
                            <ul className="m-0">
                                <li>
                                    <a
                                        href="#import-required"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Import Required
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#button-base"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Button Base
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#button-with-loader"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Button with loader
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#button-icon"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Button with Icon
                                    </a>
                                </li>
                            </ul>
                        </Sticky>
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-8">
                        <div>
                            <div
                                id="import-required"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Import Required
                                <LinkReadme id="buttons" />
                            </h1>
                            <Image
                                src="/code/Buttons/ButtonRequired.png"
                                className="m-b-100"
                            />
                        </div>
                        <div>
                            <div id="button-base" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Button Base
                                <LinkReadme id="button-base" />
                            </h1>
                            <Buttons>Button</Buttons>
                            <Image
                                src="/code/Buttons/ButtonBase.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="button-with-loader" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Button with loader
                                <LinkReadme id="button-with-loader" />
                            </h1>
                            <Buttons
                                loader={loader}
                                onClick={() => {
                                    log("onClick Button", 1);
                                    clickBtn();
                                }}
                            >
                                Button
                            </Buttons>
                            <Image
                                src="/code/Buttons/ButtonWithLoader.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="button-icon" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Button with Icon
                                <LinkReadme id="button-icon" />
                            </h1>
                            <Buttons
                                icon={
                                    <span className="m-r-10">
                                        <SVGEye />
                                    </span>
                                }
                                iconPosition="left" // left or right
                            >
                                Button
                            </Buttons>
                            <Image
                                src="/code/Buttons/ButtonWithIcon.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                    </div>
                </div>
            </Content>
        </>
    );
};
export default Index;
