import Input from "@/components/Input";
import InputPassword from "@/components/Input/Password";
import Select from "@/components/Select";
import Content from "@/components/Content";
import Sticky from "@/components/Sticky";
import Image from "@/components/Image";
import LinkReadme from "@/components/LinkReadme";

import log from "@/functions/log";

import ERRORINPUTS from "@/data/error/inputs";
import VALIDATORINPUTS from "@/data/validations/inputs";

const Index = () => {
    const exitsEmail = async (value) => {
        if (["test@gmail.com"].includes(value)) {
            return true;
        }
        return false;
    };
    const exitsName = async (value) => {
        if (["test"].includes(value)) {
            return true;
        }
        return false;
    };
    const exitsUrl = async (value) => {
        if (["https://test.com"].includes(value)) {
            return true;
        }
        return false;
    };
    return (
        <>
            <Content title="Inputs">
                <div className="container p-15 flex flex-justify-between  flex-gap-0 flex-md-gap-15">
                    <div className="flex-12 flex-md-6 flex-lg-4">
                        <Sticky className="p-v-20">
                            <ul className="m-0">
                                <li>
                                    <a
                                        href="#import_required"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Import Required
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#input_base"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Input Base
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#input_email"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Input Email
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#input_text"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Input Text
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#input_password"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Input Password
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#input_url"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Input Url
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#input_number"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Input Number
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#select"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Select
                                    </a>
                                </li>
                            </ul>
                        </Sticky>
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-8">
                        <div>
                            <div
                                id="import_required"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Import Required
                                <LinkReadme id="inputs" />
                            </h1>
                            <Image
                                src="/code/Inputs/InputRequired.png"
                                className="m-b-100"
                            />
                        </div>
                        <div>
                            <div id="input_base" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20">
                                Import Base
                            </h1>
                            <Image
                                src="/code/Inputs/InputBase.png"
                                className="m-b-100"
                            />
                        </div>
                        <div>
                            <div id="input_email" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Input Email
                                <LinkReadme id="email" />
                            </h1>
                            <Input
                                label="Email"
                                type="email"
                                placeholder="Your Email"
                                defaultValue="defaul@email.com"
                                yup={VALIDATORINPUTS.email}
                                onChange={(e) => {
                                    log("Change Email", e, "aqua");
                                }}
                                onBlur={(e) => {
                                    log("Blur Email", e, "aqua");
                                }}
                                onChangeValidate={async (value) => {
                                    log(
                                        "onChangeValidate Email",
                                        value,
                                        "aqua"
                                    );
                                    value = value.replaceAll(
                                        "yopmail.com",
                                        "gmail.com"
                                    );
                                    return value;
                                }}
                                onChangeValidateBeforeYup={async (value) => {
                                    log(
                                        "onChangeValidateBeforeYup Email",
                                        value,
                                        "aqua"
                                    );
                                    if (value.indexOf("yopmail2.com") != -1) {
                                        throw {
                                            message: ERRORINPUTS.email.invalid,
                                        };
                                    }
                                }}
                                onChangeValidateAfterYup={async (value) => {
                                    log(
                                        "onChangeValidateAfterYup Email",
                                        value,
                                        "aqua"
                                    );
                                    if (await exitsEmail(value)) {
                                        throw {
                                            message: ERRORINPUTS.email.invalid,
                                        };
                                    }
                                }}
                            />
                            <Image
                                src="/code/Inputs/InputEmail.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="input_text" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Input Text
                                <LinkReadme id="text" />
                            </h1>
                            <Input
                                label="Name"
                                type="text"
                                placeholder="Your Name"
                                defaultValue="defaul name"
                                yup={VALIDATORINPUTS.name}
                                onChange={(e) => {
                                    log("Change Name", e, "aqua");
                                }}
                                onBlur={(e) => {
                                    log("Blur Name", e, "aqua");
                                }}
                                onChangeValidate={async (value) => {
                                    log("onChangeValidate Name", value, "aqua");
                                    value = value.replaceAll(/[\d]/g, "");
                                    return value;
                                }}
                                onChangeValidateBeforeYup={async (value) => {
                                    log(
                                        "onChangeValidateBeforeYup Name",
                                        value,
                                        "aqua"
                                    );
                                    if (value.indexOf("admin") != -1) {
                                        throw {
                                            message: ERRORINPUTS.name.invalid,
                                        };
                                    }
                                }}
                                onChangeValidateAfterYup={async (value) => {
                                    log(
                                        "onChangeValidateAfterYup Name",
                                        value,
                                        "aqua"
                                    );
                                    if (await exitsName(value)) {
                                        throw {
                                            message: ERRORINPUTS.name.invalid,
                                        };
                                    }
                                }}
                            />
                            <Image
                                src="code/Inputs/InputName.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="input_password" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Input Password
                                <LinkReadme id="password" />
                            </h1>
                            <InputPassword
                                label="Password"
                                placeholder="Your Password"
                                defaultValue="defaul Password"
                                yup={VALIDATORINPUTS.password}
                                onChange={(e) => {
                                    log("Change Password", e, "aqua");
                                }}
                                onBlur={(e) => {
                                    log("Blur Password", e, "aqua");
                                }}
                                onChangeValidate={async (value) => {
                                    log("onChangeValidate Password", value, "aqua");
                                    return value;
                                }}
                                onChangeValidateBeforeYup={async (value) => {
                                    log(
                                        "onChangeValidateBeforeYup Password",
                                        value,
                                        "aqua"
                                    );
                                }}
                                onChangeValidateAfterYup={async (value) => {
                                    log(
                                        "onChangeValidateAfterYup Password",
                                        value,
                                        "aqua"
                                    );
                                }}
                            />
                            <Image
                                src="code/Inputs/InputPassword.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="input_url" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Input Url
                                <LinkReadme id="url" />
                            </h1>
                            <Input
                                label="Web Site"
                                type="url"
                                placeholder="Your Web Site"
                                defaultValue="defaul_Web_Site.com"
                                yup={VALIDATORINPUTS.url}
                                onChange={(e) => {
                                    log("Change Web Site", e, "aqua");
                                }}
                                onBlur={(e) => {
                                    log("Blur Web Site", e, "aqua");
                                }}
                                onChangeValidate={async (value) => {
                                    log(
                                        "onChangeValidate Web Site",
                                        value,
                                        "aqua"
                                    );
                                    value = value.replaceAll(/[\d]/g, "");
                                    return value;
                                }}
                                onChangeValidateBeforeYup={async (value) => {
                                    log(
                                        "onChangeValidateBeforeYup Web Site",
                                        value,
                                        "aqua"
                                    );
                                    if (value.indexOf("admin.com") != -1) {
                                        throw {
                                            message: ERRORINPUTS.url.invalid,
                                        };
                                    }
                                }}
                                onChangeValidateAfterYup={async (value) => {
                                    log(
                                        "onChangeValidateAfterYup Web Site",
                                        value,
                                        "aqua"
                                    );
                                    if (await exitsUrl(value)) {
                                        throw {
                                            message: ERRORINPUTS.url.invalid,
                                        };
                                    }
                                }}
                            />
                            <Image
                                src="code/Inputs/InputUrl.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="input_number" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Input Number
                                <LinkReadme id="number" />
                            </h1>
                            <Input
                                label="Number"
                                type="number"
                                placeholder="Your Number"
                                defaultValue="1"
                                yup={VALIDATORINPUTS.number}
                                onChange={(e) => {
                                    log("Change Number", e, "aqua");
                                }}
                                onBlur={(e) => {
                                    log("Blur Number", e, "aqua");
                                }}
                                onChangeValidate={async (value) => {
                                    log(
                                        "onChangeValidate Number",
                                        value,
                                        "aqua"
                                    );
                                    value = value.replaceAll(/[^\d]/g, "");
                                    value =
                                        value == ""
                                            ? value
                                            : Math.max(
                                                  Math.min(
                                                      parseFloat(value),
                                                      10
                                                  ),
                                                  0
                                              );
                                    return value;
                                }}
                            />
                            <Image
                                src="code/Inputs/InputNumber.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                        <div>
                            <div id="select" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Select
                                <LinkReadme id="select" />
                            </h1>
                            <Select
                                label="Options"
                                placeholder="Select"
                                defaultValue={{
                                    text: "Option 2",
                                    value: "2",
                                }}
                                yup={VALIDATORINPUTS.options}
                                options={[
                                    {
                                        text: "Option 1",
                                        value: "1",
                                        html: (
                                            <div className="flex flex-align-center">
                                                <Image
                                                    src="imageSmall.png"
                                                    className="width-20 m-r-10"
                                                />
                                                Option 1
                                            </div>
                                        ),
                                    },
                                    {
                                        text: "Option 2",
                                        value: "2",
                                    },
                                    {
                                        text: "Option 3",
                                        value: "3",
                                    },
                                    {
                                        text: "hola 3",
                                        value: "3",
                                    },
                                    {
                                        text: "ppppp 3",
                                        value: "3",
                                    },
                                    {
                                        text: "rrrr 3",
                                        value: "3",
                                    },
                                ]}
                                onChange={(e) => {
                                    log("Change Select", e, "aqua");
                                }}
                                onBlur={(e) => {
                                    log("Blur Select", e, "aqua");
                                }}
                            />
                            <Image
                                src="/code/Inputs/Select.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                    </div>
                </div>
            </Content>
        </>
    );
};
export default Index;
