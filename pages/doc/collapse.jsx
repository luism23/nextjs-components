import { useState } from "react";

import Content from "@/components/Content";
import Sticky from "@/components/Sticky";
import Collapse from "@/components/Collapse";
import LinkReadme from "@/components/LinkReadme";
import Image from "@/components/Image";

import log from "@/functions/log";

const Index = () => {
    return (
        <>
            <Content title="Collapse">
                <div className="container p-15 flex flex-justify-between  flex-gap-0 flex-md-gap-15">
                    <div className="flex-12 flex-md-6 flex-lg-4">
                        <Sticky className="p-v-20">
                            <ul className="m-0">
                                <li>
                                    <a
                                        href="#import-required"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Import Required
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="#collapse"
                                        className="flex font-18 font-montserrat m-b-20 color-black color-blue-hover"
                                    >
                                        Collapse
                                    </a>
                                </li>
                            </ul>
                        </Sticky>
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-8">
                        <div>
                            <div
                                id="import-required"
                                className="idScroll"
                            ></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Import Required
                                <LinkReadme id="collapse" />
                            </h1>
                            <Image
                                src="/code/Collapse/CollapseRequired.png"
                                className="m-b-100"
                            />
                        </div>
                        <div>
                            <div id="collapse" className="idScroll"></div>
                            <h1 className="font-25 font-montserrat m-b-20 flex flex-justify-between">
                                Collapse
                                <LinkReadme id="collapse" />
                            </h1>
                            <Collapse
                                header={
                                    <>
                                        <h3>Lorem</h3>
                                    </>
                                }
                            >
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Doloribus nemo molestias totam
                                suscipit recusandae consequatur. Itaque, impedit
                                ea cum velit, quasi eum esse harum dolores
                                perspiciatis, neque nulla cupiditate sapiente.
                            </Collapse>
                            <Image
                                src="/code/Collapse/Collapse.png"
                                className="m-t-20 m-b-100"
                            />
                        </div>
                    </div>
                </div>
            </Content>
        </>
    );
};
export default Index;
