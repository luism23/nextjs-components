export default {
    name: {
        required: "Name is Required",
        invalid: "Name invalid",
    },
    email: {
        required: "Email is Required",
        invalid: "Email invalid",
    },
    url: {
        required: "Web Site is Required",
        invalid: "Web Site Invalid",
    },
    number: {
        required: "Number is Required",
        invalid: "Number Invalid",
        min: "Number is short",
        max: "Number is long",
    },
    password: {
        required: "Password is Required",
        invalid: "Password Invalid",
        min: "Password is short",
        max: "Password is long",
    },
    options: {
        required: "Option is Required",
    },
};
