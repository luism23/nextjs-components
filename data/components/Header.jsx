export default {
    logo : "logo.png",
    nav:[
        {
            link:"/",
            text:"Home",
        },
        {
            link:"/doc",
            text:"Documentacion",
        },
        {
            link:"/doc/icons",
            text:"Icons",
        },
    ],
    nav2:[
        {
            link:"/doc/inputs",
            text:"Inputs",
        },
        {
            link:"/doc/buttons",
            text:"Buttons",
        },
        {
            link:"/doc/collapse",
            text:"Collapse",
        },
        {
            link:"/doc/progress",
            text:"Progress",
        },
    ],
}